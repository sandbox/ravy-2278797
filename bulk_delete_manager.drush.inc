<?php
/**
 * Implementation of hook_drush_command().
 */
function bulk_delete_manager_drush_command() {
  $items = array();
  $items['bulk-nodes-remove'] = array(
      'description' => "Bluk delete nodes by applying various set of conditions drush bulk-nodes-remove <node-type>",
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
      'drupal dependencies' => array('bulk_delete_manager'),
      'arguments' => array(
      'nodetypes' => 'Comma separated list of content type you wish to remove, for single node delition use "drush bulk-nodes-remove-single --help"',
      ),
    // Options for adding conditions.  
    'options' => array(
      // Condition based on menu.
      'save-menu-nodes' => array(
        'description' => 'List of menu links whose nodes are to be excluded, seperated by space if multiple',
      ),
      // Exclution based on node ids list.
      'exclude-nodes' => array(
        'description' => 'List of nodes to be excluded, seperated by comma if multiple',
      ),

      // @TODO leaving this as future enhancemant.
      // After Publish dates.
      'start-date' => array(
        'description' => 'Give a start date (dd-mm-yyyy) !!!(IN TODO LIST)!!!',
      ),
      // Before Publish dates.
      'end-date' => array(
        'description' => 'Give a end date (dd-mm-yyyy) !!!(IN TODO LIST)!!!',
      ),
    ),
  );

  $items['bulk-nodes-remove-single'] = array(
    'description' => "Node delete manager (for single node)",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments' => array(
      'nid' => array(
         'description' => 'Nid for single node (!!!(IN TODO LIST)!!!)',
       ),
    ),
  );

  return $items;
}
/**
 * Implementation of drush_hook_COMMAND_validate().
 */
function drush_bulk_delete_manager_bulk_nodes_remove_validate($nodetypes = NULL) {
  if (empty($nodetypes)) {
    drush_set_error('Please specify the node types. Comma separated list of content type you wish to remove, for single node delition use "drush mp-nodes-remove-single --help"');
  }
}

function drush_bulk_delete_manager_bulk_nodes_remove($nodetypes = array()) {
 // No time limit, allow this script to run forever if it has to
  set_time_limit(0);
  // Build array of Content Types to Delete.
  $ctypes = explode(',',$nodetypes);

  // Validate is Content Type exist.
  $available_ctypes = _node_types_build();
  drush_print('Mapping arguments provided to available content types...');
  $ignore_mmatch = FALSE;
  if ($cmatch = array_intersect($available_ctypes->names, $ctypes)) {
    drush_print('Mapping found for ' . implode(", ", $cmatch));
     $ignore_mmatch = TRUE;
    // Mismatch Found.
    if ($mmatch = array_diff($ctypes, $available_ctypes->names)) {
      drush_print('No content type match found for ' . implode(", ", $mmatch));
      if (!empty($cmatch)) {
       $ignore_mmatch = drush_confirm("Proceed excluding mismatched types?");
      }
    }
    if ($ignore_mmatch == TRUE) {
      // All Base conditions fullfilled.
      drush_print('proceeding using ' . implode(", ", $cmatch));
      // Check Conditions - menus.
      $menu_nodes = array();
      if($save_menu_nodes = drush_get_option('save-menu-nodes')) {
        $menus = explode(',', $save_menu_nodes);
        $menu_nodes = get_menu_nodes($menus);
      }
      // Check Condition - other nodes.
      $exclude_nodes = array();
      if ($save_nodes = drush_get_option('exclude-nodes')) {
        $exclude_nodes = explode(',', $save_nodes);
      }
      // Building exclude nids array.
      $excludeids = array_merge($menu_nodes, $exclude_nodes);

    }
    else {
      drush_set_error('Please enter valid content types and try again');
    }
  }
  else {
    drush_set_error('Content types not found!');
  }
  $prospect_nodes = array();
  $results = array();
  foreach ($cmatch as $ckey => $cvalue) {
    // @TODO Need to test with 2 CTs.
    $prospect_nodes = get_all_nids_from_content_type($ckey);
  }
  if ($del_nodes = nodes_saver($excludeids, $prospect_nodes)) {
    bulk_delete_nodes($del_nodes);
    drush_print('Following nids node got deleted: ' . implode(", ", $del_nodes));
    return drush_print('Job Done!');
  }
  return;
}
